import scrapy
from tencent.items import TencentItem, CommentItem
import re
import json


class NeteaseSpider(scrapy.Spider):
    name = "tencent"
    count = 0

    def start_requests(self):
        page = 1
        while page < 3:
            page += 1
            yield scrapy.Request(
                "http://www.baidu.com/s?wd={0}%20site%3Anews.qq.com&pn={1}".format("韩国", 10 * (page - 1)),
                callback=self.baidu_parse)

    def baidu_parse(self, response):

        for sec in range(2, 12):
            url = response.xpath("//*[@id=\"content_left\"]/div[{}]/h3/a/@href".format(sec)).extract_first()
            print(url)
            if url != '' and url != None:
                yield scrapy.Request(url, callback=self.news_parse)
                url = ''

    def news_parse(self, response):

        data = response.body_as_unicode()
        title = re.findall(r'title:\'(.*?)\',', data)
        ID = re.findall(r'cmt_id = (.*?);', data)


        if ID != [] and title != []:
            url = "http://coral.qq.com/article/{0}/comment?commentid=0&reqnum=30".format(ID[0])
            yield TencentItem({
                'title': title[0],
                'url': response.url,
                'ID': ID[0],
            })
            yield scrapy.Request(url, callback=self.info_parse)

    def info_parse(self, response):
        jsonData = response.body_as_unicode()
        data = json.loads(jsonData).get('data')
        if data != None:
            comments = data.get('commentid')
            last = data.get('last')
            ID = data.get('targetid')
            if comments != {} and comments != None:
                for comment in comments:
                    id = comment['id']
                    content = comment['content']
                    time = comment['time']
                    print()
                    print(CommentItem)
                    print()
                    yield CommentItem({
                        'ID': id,
                        'content': content,
                        'time': time,
                    })

            if last != "false":
                url = "http://coral.qq.com/article/{0}/comment?commentid={1}&reqnum=30".format(ID, last)
                yield scrapy.Request(url, callback=self.info_parse)


    def comment_parse(self, response):
        jsonData = response.body_as_unicode()
        data = json.loads(jsonData).get('data')
        if data != None:
            comments = data.get('commentid')
            last = data.get('last')
            ID = data.get('targetid')

            if comments != {} and comments != None:
                for comment in comments:
                    id = comment['id']
                    content = comment['content']
                    time = comment['time']
                    print()
                    print(CommentItem)
                    print()
                    yield CommentItem({
                        'ID': id,
                        'content': content,
                        'time': time,
                    })

            if last != "false":
                url = "http://coral.qq.com/article/{0}/comment?commentid={1}&reqnum=30".format(ID, last)
                yield scrapy.Request(url, callback=self.info_parse)
