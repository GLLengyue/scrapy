# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
import sqlite3
from tencent.items import CommentItem, TencentItem


class DuplicatesPipeline(object):
    def __init__(self):
        self.ids_seen = set()

    def process_item(self, item, spider):
        if item['ID'] in self.ids_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.ids_seen.add(item['id'])
            return item


class TencentPipeline(object):
    def __init__(self):
        self.conn = sqlite3.connect('Z:\\sqlite.db')
        self.c = self.conn.cursor()

    def process_item(self, item, spider):
        if isinstance(item, CommentItem):
            self.c.execute(
                "INSERT OR IGNORE into Comments values('{0}','{1}', '{2}')".format(item['ID'], item['content'],
                                                                                   item['time']))
        if isinstance(item, TencentItem ):
            self.c.execute(
                "INSERT OR IGNORE into News values('{0}','{1}', '{2}')".format(item['ID'], item['title'], item['url']))
        return item

    def close_spider(self, spider):
        self.conn.commit()
        self.conn.close()
