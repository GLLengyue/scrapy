import scrapy
from netease.items import NeteaseItem, CommentItem
import re
import json
class NeteaseSpider(scrapy.Spider):
    name = "netease"
    count = 0
    
    def start_requests(self):
        page = 1
        while page < 3:
            page += 1
            yield scrapy.Request("http://www.baidu.com/s?wd={0}%20site%3Anews.163.com&pn={1}".format("韩国", 10*(page-1)),
                                   callback = self.baidu_parse)

        
    def baidu_parse(self, response):
        
        for sec in range(2, 12):
            url = response.xpath("//*[@id=\"content_left\"]/div[{}]/h3/a/@href".format(sec)).extract_first()
            yield scrapy.Request(url, callback = self.news_parse)
            url = ''
    
    
    def news_parse(self, response):
        
        title = response.xpath("//*[@id=\"epContentLeft\"]/h1/text()").extract_first()
        ID = re.findall(r'/.*/(.*?).html', response.url)[0]
        productKey = re.findall(r'"productKey" : "(.*?)"', response.xpath("//*[@id=\"post_comment_area\"]/script[3]/text()").extract_first())[0]
        url = "http://sdk.comment.163.com/api/v1/products/{}/threads/{}".format(productKey, ID)
        yield NeteaseItem({
                    'title': title, 
                    'url': url,
                    'ID' : ID,
                    
                })
        
        yield scrapy.Request(url, callback = self.info_parse)

        
    def info_parse(self, response):
        tmp = re.findall(r'products/(.*?)/threads/(.*)', response.url)
        if tmp != []:
            productKey = tmp[0][0]
            ID = tmp[0][1]
            offset = 0
            tcount = int(re.findall(r'"tcount":(.*?),' ,response.body_as_unicode())[0])
            while offset < tcount:
                url = "http://comment.news.163.com/api/v1/products/{0}/threads/{1}/comments/newList?offset={2}&limit=30".format(productKey, ID, offset)
                yield scrapy.Request(url, callback = self.comment_parse)
                offset += 30
                
                
    def comment_parse(self, response):
        '''analyze json'''
        jsonData = re.findall(r'({.*})', response.body_as_unicode())[0]
        data = json.loads(jsonData)
        comments = data.get('comments')


        if comments != {} and comments != None:
            for commentId in comments.keys():
                content = comments[commentId]['content']
                time = comments[commentId]['createTime']
                
                yield CommentItem({
                    'ID': commentId, 
                    'content': content,
                    'time': time,
                })


            
            
        