import scrapy
from pixiv.items import PixivItem

class PixivSpider(scrapy.Spider):
    name = "pixiv"
    def start_requests(self):
        return [scrapy.Request("https://accounts.pixiv.net/login",
                                   meta={'cookiejar': 2},
                                   method='GET',
                                   callback=self.login)]
    def login(self, response):
        post_key = response.xpath("//*[@id=\"old-login\"]/form/input[1]/@value").extract_first()
        pixiv_id = "a1131554971@gmail.com"
        password = "139746852"
        return scrapy.FormRequest(
            "https://accounts.pixiv.net/login",
            formdata = {'post_key':post_key,
                        'pixiv_id':pixiv_id,
                        'password':password
            },
            meta={'cookiejar': response.meta['cookiejar']},
            callback = self.after_login)
            
    def after_login(self, response):
        return scrapy.Request("https://www.pixiv.net/ranking.php?mode=daily", meta={'cookiejar': 2})

        
    def parse(self, response):
        for section in response.xpath("//section[@class='ranking-item']"):
            yield PixivItem({
                'url': section.xpath('.//h2/a/@href').extract_first(),
                'name': section.xpath('.//h2/a/text()').extract_first(),
            })
